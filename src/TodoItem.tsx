import * as React from 'react';
import { Checkbox, Grid, Button, Input } from 'semantic-ui-react';
import './TodoItem.css';

export type TodoItem = {
  name: string,
  done: boolean
};

interface TodoItemComponentPropsCommon {
  item: TodoItem;
  key?: number | string;
  onDoneStateChange: (done: boolean) => void;
  onNameChange: (name: string) => void;
}

interface TodoItemComponentPropsIfDeleteable extends TodoItemComponentPropsCommon {
  deleteable: true;
  onDeleteButtonClick: () => void;
}

interface TodoItemComponentPropsIfNotDeletable extends TodoItemComponentPropsCommon {
  deleteable: false;
}

type ComponentState = {
  itemNameToDisplay: string;
  nameBeingEdited: boolean;
};

type ChangeNameData = {
  value: string,
  [key: string]: object | string | number | boolean
};

type ChangeDoneData = {
  checked: boolean,
  [key: string]: object | string | number | boolean
};

export type TodoItemComponentProps = TodoItemComponentPropsIfDeleteable |
  TodoItemComponentPropsIfNotDeletable;

export default class TodoItemComponent extends React.Component {
  props: TodoItemComponentProps;
  state: ComponentState;
  getNewStateAccordingToProps(props: TodoItemComponentProps): ComponentState {
    if (props.deleteable) {
      return {
        itemNameToDisplay: props.item.name,
        nameBeingEdited: false
      };
    } else {
      return {
        itemNameToDisplay: '',
        nameBeingEdited: true
      };
    }
  }
  constructor(props: TodoItemComponentProps) {
    super(props);
    this.state = this.getNewStateAccordingToProps(props);
  }
  componentWillReceiveProps(nextProps: TodoItemComponentProps) {
    this.setState(this.getNewStateAccordingToProps(nextProps));
  }

  onDoneStateChange = (event: React.SyntheticEvent<HTMLInputElement>, data: ChangeDoneData) => {
    this.props.onDoneStateChange(data.checked);
  }
  onInputValueChange = (event: React.SyntheticEvent<HTMLInputElement>, data: ChangeNameData) => {
    this.setState({itemNameToDisplay: data.value});
  }
  onBlur = (): void => {
    const nameLength: number = this.state.itemNameToDisplay.length;
    if (this.props.deleteable && (nameLength > 0)) {
      this.setState({nameBeingEdited: false});
    } else if (this.props.deleteable) {
      this.props.onDeleteButtonClick();
    }
    if (nameLength > 0) {
      this.props.onNameChange(this.state.itemNameToDisplay);
    }
  }

  render() {
    return (
      <Grid.Row className='todo-item' columns={3}>
        <Grid.Column mobile={1} tablet={1} computer={1} verticalAlign='middle'>
          <Checkbox
            checked={this.props.item.done}
            onChange={this.onDoneStateChange}
            readOnly={false}
          />
        </Grid.Column>
        <Grid.Column mobile={12} tablet={13} computer={13} verticalAlign='middle'>
        {
          this.state.nameBeingEdited ? (
            <Input
              fluid={true}
              onBlur={this.onBlur}
              onChange={this.onInputValueChange}
              placeholder='What needs to be done?'
              value={this.state.itemNameToDisplay}
            />
          ) : (
            <p
              onDoubleClick={() => this.setState({nameBeingEdited: true})}
              className={this.props.item.done ? 'done-item-label' : ''}
            >
              {this.state.itemNameToDisplay}
            </p>
          )
        }
        </Grid.Column>
        {
          this.props.deleteable && (
            <Grid.Column
              mobile={3}
              tablet={2}
              computer={2}
              verticalAlign='middle'
              textAlign='right'
            >
              <Button
                content='x'
                onClick={this.props.onDeleteButtonClick}
              />
            </Grid.Column>
          )
        }
      </Grid.Row>
    );
  }
}
