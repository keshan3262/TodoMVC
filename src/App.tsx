import * as React from 'react';
import { Button, Container, Grid, Header } from 'semantic-ui-react';
import TodoItemComponent, {
  TodoItem,
  TodoItemComponentProps
} from './TodoItem';

import './App.css';

enum FilterType {
  All, Active, Completed
}

type AppState = {
  activeFilter: FilterType,
  todoItems: TodoItem[]
};

class App extends React.Component {
  state: AppState;
  constructor(props: object) {
    super(props);
    this.state = {
      activeFilter: FilterType.All,
      todoItems: [
        { name: 'todo1', done: false },
        { name: 'todo2', done: true },
        { name: 'todo3', done: false },
        { name: 'todo4', done: true },
        { name: 'todo5', done: false }
      ]
    };
  }
  displayFilterFunction = (item: TodoItem): boolean => {
    const {activeFilter} = this.state;
    if (activeFilter === FilterType.All) {
      return true;
    } else if (activeFilter === FilterType.Active) {
      return !item.done;
    }
    return item.done;
  }
  deleteDoneItems = () => {
    this.setState({
      todoItems: this.state.todoItems.filter((item: TodoItem): boolean => !item.done)
    });
  }
  assignDoneToAll = (done: boolean) => {
    this.setState({
      todoItems: this.state.todoItems.map((item: TodoItem): TodoItem => ({ ...item, done }))
    });
  }
  addTodoItem = (name: string) => {
    this.setState({
      todoItems: [...this.state.todoItems, { name, done: false }]
    });
  }
  setItemDone = (done: boolean, itemIndex: number) => {
    this.setState({
      todoItems: this.state.todoItems.map((item: TodoItem, mappingIndex: number): TodoItem => {
        if (itemIndex === mappingIndex) {
          return { ...item, done };
        }
        return item;
      })
    });
  }
  renameItem = (newName: string, itemIndex: number) => {
    this.setState({
      todoItems: this.state.todoItems.map((item: TodoItem, mappingIndex: number): TodoItem => {
        if (itemIndex === mappingIndex) {
          return { ...item, name: newName };
        }
        return item;
      })
    });
  }
  deleteItem = (index: number) => {
    const newItems: TodoItem[] = [...this.state.todoItems];
    newItems.splice(index, 1);
    this.setState({ todoItems: newItems });
  }
  setActiveFilter = (filterType: FilterType) => {
    this.setState({activeFilter: filterType});
  }

  render() {
    const {todoItems} = this.state;
    const leftItemsCount: number = todoItems.reduce(
      (previousValue: number, currentItem: TodoItem): number => {
        return currentItem.done ? previousValue : previousValue + 1;
      },
      0
    );
    const itemWord: string = (leftItemsCount === 1) ? 'item' : 'items';

    const topItem: TodoItem = {
      done: (leftItemsCount === 0) && (todoItems.length > 0),
      name: ''
    };
    const topItemData: TodoItemComponentProps = {
      deleteable: false,
      item: topItem,
      onDoneStateChange: this.assignDoneToAll,
      onNameChange: this.addTodoItem
    };

    return (
      <div>
        <Container>
          <Header as='h1' textAlign='center'>Todos</Header>
          <Grid>
            <TodoItemComponent {...topItemData}/>
            {
              todoItems.filter(this.displayFilterFunction).map(
                (item: TodoItem, index: number): JSX.Element => {
                  const itemData: TodoItemComponentProps = {
                    deleteable: true,
                    item,
                    onDeleteButtonClick: () => this.deleteItem(index),
                    onDoneStateChange: (value: boolean) => this.setItemDone(value, index),
                    onNameChange: (name: string) => this.renameItem(name, index)
                  };
                  return (
                    <TodoItemComponent key={index} {...itemData}/>
                  );
                }
              )
            }
            <Grid.Row>
              <Grid.Column mobile='4' textAlign='left'>
                <p>{`${leftItemsCount} ${itemWord} left`}</p>
              </Grid.Column>
              <Grid.Column mobile='7' textAlign='center'>
                <Button
                  content='All'
                  onClick={() => this.setActiveFilter(FilterType.All)}
                />
                <Button
                  content='Active'
                  onClick={() => this.setActiveFilter(FilterType.Active)}
                />
                <Button
                  content='Completed'
                  onClick={() => this.setActiveFilter(FilterType.Completed)}
                />
              </Grid.Column>
              <Grid.Column mobile='5' textAlign='right'>
              {
                (leftItemsCount !== todoItems.length) && (
                  <Button
                    content='Clear completed'
                    onClick={this.deleteDoneItems}
                  />
                )
              }
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </div>
    );
  }
}

export default App;
